# whatsthescore

<img src="https://gitlab.com/tabrezakhtar/whatsthescore-web/-/raw/master/public/whatsthescore.png" alt="Image of tennis app" height="400px"/>

A simple score keeper app for forgetful tennis players!  Built with React and Sass.

To run the app:

`npm install`

`npm start`

The app will run on port 3000.

Open `localhost:3000` to run the app.

To run the tests:

`npm run tests`